//WAP to find the sum of n fractions.
#include<stdio.h>
#include<conio.h>
typedef struct
{
    int n;
    int d;
} fraction;

fraction
inputone()
{
    fraction fractionInput;
    printf("Enter the  num value: ");
    scanf("%d", &fractionInput.n);
    printf("Enter the  deno value: ");
    scanf("%d", &fractionInput.d);
    return fractionInput;
}

void input_N(int n, fraction s[n])
{
    for (int i = 0; i < n; i++)
    {
        s[i] = inputone();
    }
}

int gcd(int n, int d)
{
    if (d != 0)
        return gcd(d, n % d);
    else
        return n;
}

fraction
computeOne(fraction a, fraction b)
{
    fraction c;
    int h;
    c.n = (a.n * b.d) + (a.d * b.n);
    c.d = (a.d * b.d);
    h = gcd(c.n, c.d);
    c.n = (c.n / h);
    c.d = (c.d / h);
    return c;
}

fraction
computeN(int n, fraction s[n])
{
    fraction r;
    r.n = 0;
    r.d = 1;
    for (int i = 0; i < n; i++)
    {
        r = computeOne(r, s[i]);
    }
    return r;
}

int input_NoFraction()
{
    int n;
    printf("Enter the number of fractions we need to add: ");
    scanf("%d", &n);
    return n;
}

void displayOutput(fraction sum, int n)
{
    printf("The sum of %d fractions are  %d/%d. ", n, sum.n, sum.d);
}

int main()
{
    fraction Sum;
    int n;
    n = input_NoFraction();
    fraction m[n];
    input_N(n, m);
    Sum = computeN(n, m);
    displayOutput(Sum, n);
}