//Write a program to add two user input numbers using one function.
#include <stdio.h>
int main()
{
   int num1, num2, num3;
   printf("Enter the value of first number: ");
   scanf("%d", &num1);
   printf("Enter the value of second number: ");
   scanf("%d", &num2);
   num3 = sum(num1, num2);
   printf("The sum of the entered numbers are: %d", num3);
   return 0;
}