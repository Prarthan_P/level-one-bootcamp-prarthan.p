#include <stdio.h>
#include <math.h>

void output(float d)

{

    printf("The distance between the 2 points are: %f", d);

}

void distance(float x1, float y1, float x2, float y2)

{

    float distance;

    distance=sqrt( (x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) );

    output(distance);

}

void input()

{

    float x1, y1, x2, y2;

    printf("Enter the values of x1 , y1: ");

    scanf("%f%f", &x1, &y1);

    printf("Enter the values of x2 , y2: ");

    scanf("%f%f", &x2, &y2);

    distance(x1, y1, x2, y2);

}

int main()

{

    input();

    return 0;

}