//WAP to find the distance between two points using structures and 4 functions.
#include <stdio.h>
#include <math.h>

struct dis

{
    float x,y;
}

a,b;
int distance()

{
    float total = ((b.x - a.x) * (b.x - a.x)) + ((b.y - a.y) * (b.y - a.y));
    total = sqrt(total);
    printf("The distance between the 2 points are:%f", total);
}

int input1()

{
    printf("Enter the values of x coordinate of point 1:\n");
    scanf("%f", &a.x);
    printf("Enter the values of y coordinate of point 1:\n");
    scanf("%f", &a.y);
}

int input2()

{
    printf("Enter the values of x coordinate of point 2:\n");
    scanf("%f", &b.x);
    printf("Enter the values of y coordinate of point 2:\n");
    scanf("%f", &b.y);
}

int main()

{
    input1();
    input2();
    distance();
}
