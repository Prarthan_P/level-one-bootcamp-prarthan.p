//WAP to find the sum of two fractions.
#include<stdio.h>
#include<conio.h>
typedef struct

{
    int nume;
    int deno;
}fraction;
 
fraction sum(fraction,fraction);
 
fraction input(int x)
{
fraction f;
printf("Enter fraction %d:", x);
scanf("%d/%d", &f.nume, &f.deno);
return f;
}
int gcd(int a,int b)
{ 
    if (b != 0)
    {
    return gcd(b, a % b);
    }
    else 
    return a;
}
   fraction sum(fraction f1, fraction f2) 
{ 
  fraction sum; 
  int gcd1 = gcd(f1.deno, f2.deno);

      sum.nume = (f1.nume * f2.deno) + (f2.nume * f1.deno); 
      sum.deno = (f1.deno * f2.deno) / gcd1; 
      int last = gcd(sum.nume, sum.deno);
      sum.nume = sum.nume / last;
      sum.deno = sum.deno / last;
      return sum;  
}    
 
void output(fraction f1, fraction f2, fraction sum)
{
    printf("Addition of two fractions %d/%d + %d/%d are  %d/%d", f1.nume, f1.deno, f2.nume, f2.deno, sum.nume, sum.deno);
}
int main()
{
 fraction f1 = input(1);
 fraction f2 = input(2);
 fraction result = sum(f1, f2);
 output(f1, f2, result);  
 return 0;
}


